<div class="col-sm-3 offset-sm-1 blog-sidebar">

  <div class="sidebar-module">
    <h4>Archives</h4>

    <!-- archives is affecting posts -->

    @foreach ($archives as $stats)
    <li>
        <a href="/?month={{ $stats['month'] }}&year={{ $stats['year'] }}">
          {{ $stats['month']. ' ' . $stats['year']}}
        </a>
    </li>
    @endforeach

  </div>

  <div class="sidebar-module">
    <h4>Tags</h4>

    <!-- archives is affecting posts -->

    @foreach ($tags as $tag)
    <li>
        <a href="/posts/tag/{{ $tag }}">
          {{ $tag }}
        </a>
    </li>
    @endforeach

  </div>

</div><!-- /.blog-sidebar
