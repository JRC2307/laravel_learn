@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main">

<h1>create a post</h1>

<form method="POST" action="/posts">
  {{ csrf_field() }}

  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">body</label>
    <textarea name="body" id="body" class="form-control" required></textarea>
  </div>



  @include ('layouts.errors')


  <button type="submit" class="btn btn-primary">Publish</button>
</form>
@endsection
