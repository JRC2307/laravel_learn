@extends ('layouts.master')

@section('content')
<div class="col-sm-9 blog-main">
  <h1>{{ $post->title }}</h1>

  {{ $post->body }}
  <hr>

  <div class="comments">
    <ul class="list-group">

    @foreach ($post->comments as $comment)
    <li class="list-group-item">



      {{ $comment->body }}

    </li>
    @endforeach

  </ul>
  </div>
<!-- Add a comment -->
<div class="card">
  <div class="card-block">
    <form method="POST" action="/posts/{{ $post->id }}/comments/">
  <div class="form-group">
    <textarea name="body" placeholder="Your comment here" rows="3" cols="40"></textarea>
  </div>
<div class="form-group">
  <button type="submit" class="btn btn-primary">Add comment</button>
</div>
  </form>
  </div>

</div>
</div>

@endsection
