@extends ('layouts.master')

@section('content')
  <div class="col-md-8">
    <h1>sign In</h1>

    <form class="form-group" action="/login" method="POST">
      {{csrf_field()}}
      <div class="form-group">
        <label for="email">Email Address:</label>
        <input type="email" class="form-control" name="email" id="email">
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" name="password" value="" class="form-control">
      </div>

      <div class="form-group">
        <button type="submit" class="btn  btn-primary" name="button">Login</button>
      </div>
      @include('layouts.errors');
    </form>
  </div>
@endsection
