 <?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //given i have two records in the database that are posts,
        //and each one is posted a month apart.
        $first = factory(Post::class)->create();
        $second = factory(Post::class)->create([
          'created_at' => \Carbon\Carbon::now()->subMonth()
        ]);
        // When i fetch the archives.
        $post = Post::archives();

        // Then the response should be in the propoer format.
        $this->assertCount(2, $posts);
    }
}
